import 'dart:ui';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:tourisam/pages/LoginPage.dart';
import 'package:tourisam/pages/tabbarviewpages/karnataka.dart';
import 'package:tourisam/pages/tabbarviewpages/kerala.dart';
import 'package:tourisam/pages/tabbarviewpages/tamilnadu.dart';

class Home extends StatelessWidget {
  const Home({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 3,
        child: Scaffold(
          appBar: AppBar(
            actions: [
              TextButton(
                  onPressed: () async {
                    await signoutt();

                    Navigator.of(context).pushReplacement(
                      MaterialPageRoute(builder: (context) => Login()),
                    );
                  },
                  child: Text(
                    'Log out',
                    style: TextStyle(color: Colors.white, fontSize: 16),
                  ))
            ],
            backgroundColor: Colors.indigoAccent,
            leading: Icon(Icons.location_on, size: 35),
            title: Text(
              'Goo!',
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 30,
                  fontWeight: FontWeight.bold),
            ),
            bottom: TabBar(
              labelColor: Colors.white,
              tabs: [
                Tab(
                  child: Text(
                    "Kerala",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                Tab(
                  child: Text(
                    "Tamilnadu",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                Tab(
                  child: Text(
                    "Karnataka",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ],
            ),
          ),
          body: TabBarView(children: [Kerala(), Tamilnadu(), Karnataka()]),
        ));
  }

  Future signoutt() async {
    await GoogleSignIn().signOut();
  }
}
