import 'package:flutter/material.dart';
import 'package:tourisam/pages/expansiontilecard.dart';

class Kerala extends StatelessWidget {
  const Kerala({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        TileCard(
            maintitile: "Kochi",
            subtiti: "Kearala Best Place",
            newimages: "asstes/newpic/Cochin.jpg",
            discription:
                "Kochi or Cochin is a unique placein God’s Own Country that is perfectly blended with cultural values and modern ideas! Fondly called the ‘Queen of the Arabian Sea’, this imposing port-city has always been an attraction point since the beginning of history"),
        TileCard(
            maintitile: "Munnar",
            subtiti: "Munnar Kearala Best Place ",
            newimages: "asstes/newpic/munnar-1.png",
            discription:
                "Yet another gorgeous hill station in the lap of the fascinating Western Ghats, Munnar needs no introduction. Rising 1,600m above the sea level, a vacation in the beguiling locales of this hilly retreat is all about the lofty clouds, picturesque mountains, rolling hills, and a soothing ambience"),
        TileCard(
            maintitile: "Wayanad",
            subtiti: "Wayanad Is Best Place in Kerala ",
            newimages: "asstes/newpic/Wayanad.jpg",
            discription:
                "Cradled in the lap of the Western Ghats, Wayanad truly deserves a top rank in the list of places to visit in Kerala. Situated at an astounding height of 700-2,100m, this stunning hill station is all about fantasy, untouched nature, unforgettable moments, and merriment!"),
        TileCard(
            maintitile: "Alappuzha",
            subtiti: " most popular backwater destinations in Kerala",
            newimages: "asstes/newpic/Alleppey.jpg",
            discription:
                "There is the whole of Kerala in one side, and then there is this heavenly tourist destination called Alappuzha or Alleppey! Esteemed as the ‘Backwater Capital of India’ or the ‘Venice of the East’, Alleppey is known for its silent backwaters and bountiful beauty!"),
        TileCard(
            maintitile: "Idukki",
            subtiti: "Idukki Is Most Lovable Place In Kerala",
            newimages: "asstes/newpic/Idukki.jpg",
            discription:
                "Home to numerous natural marvels, lofty peaks like Anamudi and others, captivating sanctuaries, and spice gardens, this surreal part of Kerala is a must for every nature lovers. To note here, many of the popular Kerala tourist destinations like Munnar, Vagamon, Ramakkalmedu, Marayur, and Thekkady are located within this district.")
      ],
    );
  }
}
