import 'package:flutter/material.dart';
import 'package:tourisam/pages/expansiontilecard.dart';

class Tamilnadu extends StatelessWidget {
  const Tamilnadu({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        TileCard(
            maintitile: "Hogenakkal",
            subtiti: "Gorgeous Waterfalls",
            newimages: "asstes/newpic/Hogenakkal .jpg",
            discription:
                "all the best tourist places in Tamil Nadu, Hogenakkal is a hometown of gorgeous waterfalls. Situated in Dharampuri, this small village is an absolute hotspot for romantic getaways, a short family vacay, and even an offbeat trip with your friends."),
        TileCard(
            maintitile: "Dhanushkodi",
            subtiti: "Beautiful Abandoned Town",
            newimages: "asstes/newpic/Dhanushkodi.jpg",
            discription:
                "If you are someone who’s on a constant lookout for places that are away from the city life, Dhanushkodi is one of the best places to visit in Tamil Nadu. This beach town’s grandeur and beauty make Dhanushkodi one of the best tourist places in Tamil Nadu for your next refreshing getaway!"),
        TileCard(
            maintitile: "Mudumalai",
            subtiti: "Best Ancient Hills In Tamilnadu",
            newimages: "asstes/newpic/Mudumalai.jpg",
            discription:
                "Located in the Nilgiri hills, Mudumalai is one of the best tourist places in Tamil Nadu, which is known for its wildlife persona. For anyone who loves an encounter with nature in the most unique way, the national park here is a must visit."),
        TileCard(
            maintitile: "Pondicherry",
            subtiti: "French Colony In Tamilnadu",
            newimages: "asstes/newpic/Pondicherry.jpg",
            discription:
                " this Union Territory reeks of excitement and vibrancy. If you have had too much of Goa already or even if you haven’t, it’s time you witness the charm of Pondy! In addition, traveling from Chennai to Pondicherry has been made much more easier and this is what attracts the young crowd to one of the best tourist places in Tamil Nadu and the land of beautiful beaches to rejuvenate and relax."),
        TileCard(
            maintitile: "Trichy",
            subtiti: "Historical Temples In Tamilnadu",
            newimages: "asstes/newpic/Trichy.jpeg",
            discription:
                "Trichy is one of the fastest emerging cities in Tami Nadu that can be easily explored in a weekend. It offers multiple places for tourists to visit, like temples, churches. Each brings an amazing uniqueness with it that will be etched in your heart for a long time. It is one of the best tourist places in Tamil Nadu."),
      ],
    );
  }
}
