import 'package:flutter/material.dart';
import 'package:tourisam/pages/expansiontilecard.dart';

class Karnataka extends StatelessWidget {
  const Karnataka({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        TileCard(
            maintitile: "Udupi",
            subtiti: "Udupi is a coastal town in Karnataka",
            newimages: "asstes/newpic/UDUPI.jpg",
            discription:
                "Udupi is a coastal town in Karnataka, most famous for its vegetarian cuisine and South Indian restaurants all over the country. With beautifully carved ancient temples, laidback beaches and unexplored forests, Udupi is also the place where the educational hub of Manipal is located. Situated 60 km from Mangalore, Udupi is famous for its temple culture and beautiful beaches."),
        TileCard(
            maintitile: "Mysore",
            subtiti: "Mysore known as The City of Palaces ",
            newimages: "asstes/newpic/MYSORE.jpg",
            discription:
                "Famously known as The City of Palaces, it wouldn’t be wrong to say that Mysore, currently Mysuru, is one of the most important places in the country regarding ancient reigns. It is replete with the history of its dazzling royal heritage, intricate architecture, its famed silk sarees, yoga, and sandalwood, to name just a few. Located in the foothills of the Chamundi Hills, Mysore is the third most populated city in Karnataka, and its rich heritage draws "),
        TileCard(
            maintitile: "Badami",
            subtiti: "Located in a valley of rugged red sandstone ",
            newimages: "asstes/newpic/BADAMI.jpg",
            discription:
                "Located in a valley of rugged red sandstone, surrounding the Agastya Lake, Badami (formerly known as Vatapi) is an archaeological delight owing to its beautifully crafted sandstone cave temples, fortresses and carvings. Located in the Bagalkot district in Karnataka, Badami is part of the UNESCO World Heritage Sites that constitute Aihole-Badami-Pattadakal and is the finest example of traditional temple architecture in India."),
        TileCard(
            maintitile: "Coorag",
            subtiti: " Located amidst imposing mountains in Karnataka",
            newimages: "asstes/newpic/COORG.jpg",
            discription:
                "Located amidst imposing mountains in Karnataka with a perpetually misty landscape, Coorg is a popular coffee producing hill station. It is popular for its beautiful green hills and the streams cutting right through them. It also stands as a popular destination because of its culture and people. The Kodavas, a local clan specializing in martial arts, are especially notable for their keen hospitality."),
        TileCard(
            maintitile: "Hampi",
            subtiti:
                "Hampi, the city of ruins, is a UNESCO World Heritage Site.",
            newimages: "asstes/newpic/HAMPI.jpg",
            discription:
                "Hampi, the city of ruins, is a UNESCO World Heritage Site. Situated in the shadowed depth of hills and valleys in the state of Karnataka, this place is a historical delight for travellers. Surrounded by 500 ancient monuments, beautiful temples, bustling street markets, bastions, treasury building and captivating remains of Vijayanagar Empire, Hampi is a backpacker's delight"),
      ],
    );
  }
}
