import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:firebase_core/firebase_core.dart';

class Login extends StatefulWidget {
  const Login({Key? key}) : super(key: key);

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Align(
            alignment: Alignment.topCenter,
          ),
          Container(
            height: 311.2,
            decoration: BoxDecoration(color: Colors.indigoAccent),
            child: Padding(
              padding: const EdgeInsets.only(left: 35, right: 35, top: 55),
              child: Column(
                children: [
                  Center(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.location_on,
                          color: Colors.white,
                          size: 80,
                        ),
                      ],
                    ),
                  ),
                  Text(
                    "Goo!",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 50,
                        fontWeight: FontWeight.bold),
                  )
                ],
              ),
            ),
          ),
          Align(
            alignment: Alignment.bottomLeft,
            child: Container(
                height: 580,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(50),
                        topRight: Radius.circular(50))),
                child: Column(
                  children: [
                    Padding(
                      padding:
                          const EdgeInsets.only(left: 15, top: 35, right: 15),
                      child: Row(
                        children: [
                          Text(
                            'Welcome!',
                            style: TextStyle(
                                color: Colors.indigoAccent,
                                fontSize: 30,
                                fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                    ),
                    Forms()
                  ],
                )),
          ),
        ],
      ),
    );
  }
}

class Forms extends StatefulWidget {
  const Forms({Key? key}) : super(key: key);

  @override
  _FormsState createState() => _FormsState();
}

class _FormsState extends State<Forms> {
  GlobalKey<FormState> formkey = GlobalKey();
  final _emailcomtroller = TextEditingController();
  final _passwordcontroller = TextEditingController();
  dynamic _value = false;
  @override
  Widget build(BuildContext context) {
    return Form(
      key: formkey,
      child: Padding(
        padding: const EdgeInsets.only(left: 25, top: 35, right: 25),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text('Login'),
            TextFormField(
              controller: _emailcomtroller,
              validator: (value) => value!.isEmpty ? "E-Mail Is Empty" : null,
              decoration: InputDecoration(hintText: 'Please Enter E-Mail'),
              keyboardType: TextInputType.emailAddress,
            ),
            SizedBox(height: 20),
            Text('Password'),
            TextFormField(
                obscureText: true,
                controller: _passwordcontroller,
                validator: (value) => value!.length < 8 || value.length > 15
                    ? "password min 8"
                    : null,
                keyboardType: TextInputType.visiblePassword,
                decoration: InputDecoration(hintText: 'Please Enter Password')),
            SizedBox(
              height: 9,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                TextButton(
                  onPressed: () {},
                  child: Text('Forget Password ?',
                      style: TextStyle(color: Colors.black)),
                )
              ],
            ),
            SizedBox(height: 7),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Checkbox(
                    checkColor: Colors.white,
                    activeColor: Colors.indigoAccent,
                    value: _value,
                    onChanged: (newvalue) {
                      setState(() {
                        _value = newvalue;
                      });
                    }),
                Text('Terms & Condition', style: TextStyle(color: Colors.black))
              ],
            ),
            SizedBox(
              height: 13,
            ),
            Center(
              child: Container(
                width: 250,
                height: 40,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(50),
                ),
                child: ElevatedButton.icon(
                    style: ElevatedButton.styleFrom(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30)),
                      primary: Colors.indigoAccent,
                    ),
                    onPressed: () {
                      setState(() {
                        if (formkey.currentState!.validate()) {
                          Navigator.pushNamed(context, 'home');
                        }
                      });
                    },
                    icon: Icon(Icons.login),
                    label: Text('Login',
                        style: TextStyle(
                          fontSize: 15,
                        ))),
              ),
            ),
            SizedBox(
              height: 26,
            ),
            Center(
              child: Container(
                width: 250,
                height: 40,
                child: ElevatedButton.icon(
                    style: ElevatedButton.styleFrom(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30)),
                      primary: Colors.indigoAccent,
                    ),
                    onPressed: () async {
                      await signInWithGoogle();
                      setState(() {});
                    },
                    icon: Icon(
                      FontAwesomeIcons.google,
                      color: Colors.red[400],
                    ),
                    label: Text('Google Sign In')),
              ),
            )
          ],
        ),
      ),
    );
  }

  Future<UserCredential> signInWithGoogle() async {
    // Trigger the authentication flow
    final GoogleSignInAccount? googleUser = await GoogleSignIn().signIn();

    // Obtain the auth details from the request
    final GoogleSignInAuthentication? googleAuth =
        await googleUser?.authentication;

    // Create a new credential
    final credential = GoogleAuthProvider.credential(
      accessToken: googleAuth?.accessToken,
      idToken: googleAuth?.idToken,
    );
    Navigator.pushNamed(context, "home");

    // Once signed in, return the UserCredential
    return await FirebaseAuth.instance.signInWithCredential(credential);
  }
}
