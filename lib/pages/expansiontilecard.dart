import 'package:flutter/material.dart';

class TileCard extends StatelessWidget {
  final String maintitile;
  final String subtiti;
  final newimages;
  final String discription;

  const TileCard(
      {required this.maintitile,
      required this.subtiti,
      required this.newimages,
      required this.discription});

  @override
  Widget build(BuildContext context) {
    return ExpansionTile(
      title: Text(
        maintitile,
        style: TextStyle(fontWeight: FontWeight.bold, fontSize: 21),
      ),
      subtitle: Text(subtiti, style: TextStyle(color: Colors.grey)),
      children: [
        Divider(),
        Image.asset(newimages),
        SizedBox(
          height: 10,
        ),
        Row(
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                'Discription',
                style: TextStyle(fontSize: 19, fontWeight: FontWeight.bold),
              ),
            ),
          ],
        ),
        SizedBox(
          height: 3,
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(discription),
        )
      ],
    );
  }
}
