import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:tourisam/pages/LoginPage.dart';
import 'package:tourisam/pages/homepage.dart';
import 'package:animated_splash_screen/animated_splash_screen.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized;
  Firebase.initializeApp();
  runApp(First());
}

class First extends StatelessWidget {
  const First({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        theme: ThemeData(primarySwatch: Colors.indigo),
        debugShowCheckedModeBanner: false,
        home: AnimatedSplashScreen(
          duration: 2500,
          splash: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Icon(Icons.location_on, color: Colors.indigoAccent, size: 80),
              Text(
                'Goo!',
                style: TextStyle(
                    color: Colors.indigoAccent,
                    fontSize: 50,
                    fontWeight: FontWeight.bold),
              )
            ],
          ),
          nextScreen: Login(),
          splashTransition: SplashTransition.scaleTransition,
        ),
        routes: {
          "home": (context) {
            return Home();
          },
          "login": (context) {
            return Login();
          }
        });
  }
}
